/* eslint-disable no-useless-escape */
import React,{ useState} from 'react'
import { Grid,Segment,Header, Button, Message, Form } from 'semantic-ui-react'
import {Link, useHistory } from "react-router-dom"
import axios from "axios";
import cogoToast from 'cogo-toast';

const options = {
  headers: {'Content-Type': 'application/json',
  "Authorization": `Bearer ${localStorage.getItem('jwt')}`
},
};

const Login = () => {
  const history = useHistory();
  const [password,setPassword] = useState('');
  const [email,setEmail] = useState('');

  const login= async () => {
    if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)){
     return cogoToast.error("invalid email");
    }
    try {
        const {data} = await axios.post(`${process.env.REACT_APP_URL}/login`,
        {password,email}, options)
        localStorage.setItem('jwt',data.token)
        localStorage.setItem('user',JSON.stringify(data.data))
        cogoToast.success(<div>{data.message}</div>);
        console.log(data)
        history.push('/');
    } catch (error) {
        const {data} = error.response;
        console.log(data)
        cogoToast.error(<div>{data.data}</div>);
    }
}
  return(
  
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
    <Grid.Column style={{ maxWidth: 450 }}>
    <Header as='h2' color='teal' textAlign='center'>
         Log-in to your account
      </Header>
      <Form size='large'>
        <Segment stacked>
        <Form.Input 
          fluid icon="at" iconPosition='left' placeholder="E-mail address"
          type="text" 
          value={email}
          onChange={(e)=>setEmail(e.target.value)}
          />
          <Form.Input
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Password'
            type='password'
            value={password}
           onChange={(e)=>setPassword(e.target.value)}
          />
          <Button color='teal' fluid size='large'
          onClick={()=>login()} 
          >
            Login
          </Button>
        </Segment>
      </Form>
      <Message>
        New to us? <Link to='/signup'>Sign Up</Link>
      </Message>
    </Grid.Column>
  </Grid>
)
  }

export default Login
