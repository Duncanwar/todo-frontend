import React, { useState,  useEffect } from 'react';
import { Card, Icon,Popup, Button,Modal,Form,Segment, TextArea,Header } from 'semantic-ui-react'
import axios from "axios";
import cogoToast from "cogo-toast"

const options = {
  headers: {'Content-Type': 'application/json',
  "Authorization": `Bearer ${localStorage.getItem('jwt')}`
},
};

function exampleReducer(state, action) {
  switch (action.type) {
    case 'close':
      return { open: false }
    case 'open':
      return { open: true, size: action.size }
    default:
      throw new Error('Unsupported action...')
  }
}
const Home = () => {
  const [state, dispatch] = React.useReducer(exampleReducer, {
    open: false,
    size: undefined,
  })
  const { open, size } = state
  const [todo,setTodo] = useState([])
  const [title,setTitle] = useState('')
  const [description,setDescription] = useState('');
  const [update,setUpdate] = useState(false);
  useEffect(()=>{
  setUpdate(false)
   mytodos();
  },[update])
const mytodos = async () => {
  try {
      const {data} = await axios.get(`${process.env.REACT_APP_URL}/todos/user`,options
      );
setTodo(data.data);
  } catch (error) {
    console.log(error);
      const {data} = error.response;
      console.log(data)
      cogoToast.error(<div>{data.data}</div>);
      console.log(error.response.data)
      cogoToast.info(<div>{data.data}me</div>);
  }
}

const deleteTodo = async (id) => {
  try {
    const {data} = await axios.delete(`${process.env.REACT_APP_URL}/todos/${id}`,
    options
    )
    cogoToast.success(<div>{data.message}</div>);
    setUpdate(true);
  } catch (error) {
    const {data} = error.response;
    cogoToast.error(<div>{data.data}</div>);
  }
}
const add = async () => {
  try {
    const {data} = await axios.post(`${process.env.REACT_APP_URL}/todos`,
    {title,description},
    options)
    setUpdate(true);
    setDescription('');
    setTitle('');
    dispatch({ type: 'close' })
    cogoToast.success(<div>{data.message}</div>);    
  } catch (error) {
    const {data} = error.response;
    cogoToast.error(<div>{data.data}</div>);
  }
}
    return (
        <>
        {todo.length === 0 ?<>
          <Segment placeholder>
    <Header icon>
      <Icon name='list file outline' />
      No Todo List Added Here.
    </Header>
    <Button primary>
    <Popup content='Add New todo on your feed' trigger={<Button primary icon='add'
         onClick={() => dispatch({ type: 'open', size: 'mini' })}
        />}
        />
    </Button>
  </Segment>
  <Modal
        size={size}
        open={open}
        onClose={() => dispatch({ type: 'close' })}
      >
        <Modal.Header>Add New Todo List</Modal.Header>
        <Modal.Content>
        <Form size='large'>
        <Segment stacked>
        <Form.Input reset
          fluid  iconPosition='left' placeholder="Todo Title"
          type="text" 
          value={title}
          onChange={(e)=>setTitle(e.target.value)}
          />
          <TextArea
            fluid
            placeholder='describe'
            value={description}
           onChange={(e)=>setDescription(e.target.value)}
          />

        </Segment>
      </Form>
      
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={() => dispatch({ type: 'close' })}>
            cancel
          </Button>
          <Button icon="add" positive onClick={() => add() }>
            Add
          </Button>
        </Modal.Actions>
      </Modal>
        
        </>:
        <>
         <Popup content='Add New todo on your feed' trigger={<Button icon='add'
         onClick={() => dispatch({ type: 'open', size: 'mini' })}
        />}
        />
        <Modal
        size={size}
        open={open}
        onClose={() => dispatch({ type: 'close' })}
      >
        <Modal.Header>Add New Todo List</Modal.Header>
        <Modal.Content>
        <Form size='large'>
        <Segment stacked>
        <Form.Input reset
          fluid  iconPosition='left' placeholder="Todo Title"
          type="text" 
          value={title}
          onChange={(e)=>setTitle(e.target.value)}
          />
          <TextArea
            fluid
            placeholder='describe'
            value={description}
           onChange={(e)=>setDescription(e.target.value)}
          />

        </Segment>
      </Form>
      
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={() => dispatch({ type: 'close' })}>
            cancel
          </Button>
          <Button icon="add" positive onClick={() => add() }>
            Add
          </Button>
        </Modal.Actions>
      </Modal>
          {todo.map((item,i) =>{
            return (
              <Card className="center">
              <Card.Content>
      <Card.Header >{item.User.name}
      <Icon name="delete" iconPosition="left"
      onClick={()=>deleteTodo(item.id)}
      />
      
      </Card.Header>
      <hr/>
      <Card.Header size="small">
        <span >{item.title}</span>
      </Card.Header>
      <Card.Description>
       {item.description}
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <span className='date'>
        <Icon name='time' />
        createdAt {new Date(item.createdAt).toDateString()} 
        {new Date(item.createdAt).toLocaleTimeString()}
      </span>
    </Card.Content>
    </Card>
            )
          })}
        </>
        }
          </>
    );
};

export default Home;