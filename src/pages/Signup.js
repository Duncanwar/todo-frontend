/* eslint-disable no-useless-escape */
import React, { useState } from 'react'
import { Grid,Segment,Header, Button, Message, Form } from 'semantic-ui-react'
import {Link, useHistory} from "react-router-dom"
import axios from "axios";
import cogoToast from 'cogo-toast';

const Signup = () => { 
  const history = useHistory();
  const [name,setName] = useState('');
  const [password,setPassword] = useState('');
  const [email,setEmail] = useState('');

  const signup= async () => {
    if(/[^a-zA-Z]$/.test(name)){
      return cogoToast.error("Must be characters only");
     }
    if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)){
      return cogoToast.error("invalid email");
     }
      try {
          const {data} = await axios.post(`${process.env.REACT_APP_URL}/signup`,
          {name,password,email}
          );
        cogoToast.success(<div>{data.message}</div>);
        history.push('/login');

      } catch (error) {
          const {data} = error.response;
          cogoToast.error(<div>{data.data}</div>);
      }
  }
    return(
    <>
<Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
    <Grid.Column style={{ maxWidth: 450 }}>
    <Header as='h2' color='teal' textAlign='center'>
       Signup To todo App
      </Header>
      <Form size='large'>
        <Segment stacked>
        <Form.Input  fluid icon='user' iconPosition='left' placeholder='Your fullname'
        type='text'
        value={name}
        onChange={(e)=>setName(e.target.value)}
        />
        <Form.Input 
          fluid icon="at" iconPosition='left' placeholder="E-mail address"
          type="text" 
          value={email}
          onChange={(e)=>setEmail(e.target.value)}
          />
          <Form.Input
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Password'
            type='password'
            value={password}
           onChange={(e)=>setPassword(e.target.value)}
          />

          <Button color='teal' fluid size='large' 
          onClick={()=> signup()}
          >
            Signup
          </Button>
        </Segment>
      </Form>
      <Message>
        With to us? <Link to='/login'>Login</Link>
      </Message>
    </Grid.Column>
  </Grid>
    </>
  )}

export default Signup
