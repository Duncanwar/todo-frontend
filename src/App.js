import './App.css';
import React,{useEffect} from 'react'
import {BrowserRouter as Router , Switch, Route, useHistory } from "react-router-dom";
import Signup from "./pages/Signup";
import Login from "./pages/Login";
import Home from "./pages/Home";
import AppShell from './AppShell';

const Routing = () => {
  const history = useHistory();
  useEffect(()=>{
    const user = JSON.parse(localStorage.getItem('user'));
    if(user){
      history.push('/');
    }else {
      history.push('/login');
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return(
    <Switch>
      <Route path="/Signup">
<Signup />
      </Route>
      <Route path="/login">
<Login />
      </Route>
      <Route exact path="/">
        <AppShell >
          <Home />
          </AppShell>
      </Route>
      </Switch>
  )
}
const App=()=> {
  return (
    <div className="App">
      <Router>
        <Routing />
      </Router>
    </div>
  );
}

export default App;
