import React, { useState } from 'react'
import { Menu } from 'semantic-ui-react'
import { useHistory} from "react-router-dom";

const Navbar = () => {
  const history = useHistory();
  const [state,setState] = useState({
      activeItem: 'home'
  })
const logout = async () => {
  try {
    localStorage.removeItem('user')
    localStorage.removeItem('jwt');
    history.push('/login')
} catch (error) {
throw new Error(error)
}
}
    return (
      <div>
        <Menu pointing secondary>
          <Menu.Item link
            name='home'
            active={state.activeItem === 'home'}
          />
          <Menu.Menu position='right'>
            <Menu.Item
              name='logout'
              onClick={()=>logout()}
            />
          </Menu.Menu>
        </Menu>
      </div>
    )
  }
export default Navbar;