import React from "react";
import Navbar from "./components/Navbar";
import { Grid } from 'semantic-ui-react'

const  AppShell= ({ children } ) => {
    return (
      <>
        <Navbar />
            <Grid centered columns='equal'>
    <Grid.Column/>
    <Grid.Column  width={5}>
      <div >{children}</div>
    </Grid.Column>
    <Grid.Column/>
  </Grid>
      </>
    );
  }

export default AppShell;
